FROM continuumio/miniconda3

WORKDIR /app

# Install myapp requirements
#COPY environment.yml /app/environment.yml
#RUN conda env create -n acc -f environment.yml \
#    && rm -rf /opt/conda/pkgs/*

## activate the myapp environment
#ENV PATH /opt/conda/envs/acc/bin:$PATH


COPY requirements.txt .

RUN conda install -y -c conda-forge -c bioconda --file requirements.txt
