Create a conda environment and a docker container to store all applications and libraries used for the ACC pipelines.

It is reccomended  to use as much as possible the docker container, converted if necessary to a singularity image. Whenever it is not possible, we can create a conda environment.


Create a conda environment:

```bash
conda create -y -n acctoolkit
source activate acctoolkit
conda install -y --file requirements.txt
```

---- 

## CINECA/Galileo: ##

To use conda, just add it to your path by adding the following line in `~/.bashrc`:

```export PATH="/gpfs/work/ELIX_ACC-Bio_1/software/minconda3/bin:$PATH"```

The environment has been installed in ```$WORK/envs/accenv```, to activate it:

```source activate $WORK/envs/acctoolbox```


----



Create a docker container from source:

```bash
docker build -t acc-bioinfo .
```

Or pull it from the ACC repository:

```bash
docker pull accbioinfo/acctoolkit
```

Run a program with docker:

```bash
docker run -ti --rm acctoolkit samtools
```


Convert to singularity:

```bash
docker run -v /var/run/docker.sock:/var/run/docker.sock  -v `pwd`:/output  --privileged -t --rm  singularityware/docker2singularity acctoolkit
```
This command will generate a `.img` file that can be copied on a remote server and used with singularity.

Run a command with singularity:

```bash
singularity exec acctoolkit.img samtools
```

You will need to map the directories where the files to analyze are stored, for instance:

```bash
singularity exec -bind /gpfs/work/ELIX_ACC-Bio_1/singularity/acctoolkit-<version>.img samtools
```


---- 

## CINECA/Marconi: ##

Note: on Galileo at  CINECA, the singularity image is stored in ```$WORK/singularity/```.

Run it:

```bash
# Bind the work directory to make it available to singularity
singularity exec -bind /gpfs/work/ELIX_ACC-Bio_1/ $WORK/singularity/acc-bioinfo-<version>.img samtools
```

----


## Example commands:

# Index bam files:

`singularity exec --bind `pwd` $WORK/singularity/acc-bioinfo-2018-07-10-3be6f4005ede.img  samtools index *.bam`

